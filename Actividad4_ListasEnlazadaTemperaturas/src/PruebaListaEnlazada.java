
public class PruebaListaEnlazada {
	public static void main(String[]args) {
		ListaEnlazada le = new ListaEnlazada();
		
		Temperatura t1 = new Temperatura(57.1, "01/10/2019");
		Temperatura t2 = new Temperatura(20.2, "02/10/2019");
		Temperatura t3 = new Temperatura(30.9, "03/10/2019");
		Temperatura t4 = new Temperatura(12.8, "04/10/2019");
		Temperatura t5 = new Temperatura(09.7, "31/10/2019");
		Temperatura t6 = new Temperatura(16.4, "30/10/2019");
		
		le.agregarElementoAlFinal(t1);
		le.agregarElementoAlFinal(t2);
		le.agregarElementoAlFinal(t3);
		le.agregarElementoAlFinal(t4);
		le.agregarElementoAlFinal(t5);
		le.agregarElementoAlFinal(t6);
		
		System.out.println("======== Promedio de todas las temperaturas ========");
		System.out.println(le.promedioTemperaturas());
		System.out.println("=======================");
		System.out.println("======== Promedio de las temperaturas entre los dias dados ========");
		System.out.println(le.promedioTemperaturasEspecifico("01/10/2019", "02/10/2019")); // funciona con dias **
	}
}
