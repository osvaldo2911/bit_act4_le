class Temperatura{
	private double temperatura;
	private String fechaDeRegistro;
	public Temperatura(double temperatura, String fechaDeRegistro) {
		super();
		this.temperatura = temperatura;
		this.fechaDeRegistro = fechaDeRegistro;
	}
	public double getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(double temperatura) {
		this.temperatura = temperatura;
	}
	public String getFechaDeRegistro() {
		return fechaDeRegistro;
	}
	public void setFechaDeRegistro(String fechaDeRegistro) {
		this.fechaDeRegistro = fechaDeRegistro;
	}
	@Override
	public String toString() {
		return "temperatura [temperatura=" + temperatura + ", fechaDeRegistro=" + fechaDeRegistro + "]";
	}
}
public class Tem {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
