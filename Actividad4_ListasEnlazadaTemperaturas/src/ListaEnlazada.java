public class ListaEnlazada {

	Nodo nodoInicio;
	Nodo nodoFin;

	//1) Crear lista enlazada
	public ListaEnlazada() {
		nodoInicio = null;
		nodoFin = null;
	}

	// Lista Vacia
	public boolean vacia() {
		return nodoInicio ==null?true:false;
	}

	//2a)Agregar elemento al inicio
	public void agregarElementoAlInicio(Temperatura dato){
		Nodo nodoNuevo = new Nodo(dato);



		if (nodoInicio == null) {
			nodoFin = nodoInicio = nodoNuevo;		
		}else {
			nodoNuevo.setEnlace(nodoInicio);
			nodoInicio = nodoNuevo;
		}

	}
	//2b)Agregar elemento al final
	public void agregarElementoAlFinal(Temperatura dato){
		Nodo nodoNuevo = new Nodo(dato);

		if (nodoFin == null) {
			nodoFin = nodoInicio = nodoNuevo;
		}else {
			nodoFin.setEnlace(nodoNuevo);
			nodoFin = nodoNuevo;
		}

	}

	// Agregar Elemento especifico


	//3a)Eliminar elemento al inicio
	public void eliminarElementoAlInicio() {
		if (nodoInicio == null) {

		}else {
			nodoInicio = nodoInicio.getEnlace();
		}
	}

	//3b)Eliminar elemento al Final
	public void eliminarElementoAlFIn() {
		Nodo aux = nodoInicio;
		if (nodoFin == null) {

		}else {
			while(aux.getEnlace()!=null){
				nodoFin = aux;
				aux=aux.getEnlace();
			}
			nodoFin.setEnlace(null);
		}
	}

	// Eliminar Dato especifico
	public Temperatura eliminarDatoEspecifico(Temperatura dato) {

		if (vacia()) {
			System.out.println("No hay elementos a eliminar");
			return null;
		} else if(nodoInicio.getDato() == dato){
			System.out.println("Encontrado en el primer nodo");
			Temperatura d = nodoInicio.getDato();
			nodoInicio = nodoInicio.getEnlace();
			//nodoInicio = null;
			return d;
		}else if(nodoInicio == nodoFin) {
			return null;
		}else {
			Nodo nodoAnterior, nodoSiguiente;
			nodoAnterior = nodoInicio;
			nodoSiguiente = nodoInicio.getEnlace();
			while (nodoSiguiente.getDato() != dato && nodoSiguiente.getEnlace() != null ) {
				nodoAnterior = nodoAnterior.getEnlace();
				nodoSiguiente = nodoSiguiente.getEnlace();
			}

			if(nodoSiguiente.getDato() == dato) {
				nodoAnterior.setEnlace(nodoSiguiente.getEnlace());
				return nodoSiguiente.getDato();
			}else {
				return null;
			}	
		}
	}
	//Recorrer Lista
	public int recorrerLista() {
		Nodo au = nodoInicio;
		int cont = 0;
		while(au!=null){
			au=au.getEnlace();
			cont++;
		}
		return cont;
	}

	//Mostrar Elementos
	public void mostrarElementos() {
		Nodo nodoActual = nodoInicio;

		while (nodoActual != null) {
			System.out.print("["+nodoActual.getDato()+"]");
			nodoActual = nodoActual.getEnlace();
		}
	}

	//Promedio Temperaturas
	public String promedioTemperaturas() {
		Nodo nodoActual = nodoInicio;
		String res = "Error";
		int cont = recorrerLista();
		double sum = 0;
		double pro = 0;
		while (nodoActual != null) {
			sum = sum + nodoActual.getDato().getTemperatura();
			nodoActual = nodoActual.getEnlace();
		}
		pro = sum/cont;
		
		if (pro>0 && pro<=10) {
			return res = pro +" |> Congelacion";
		}else if (pro>10.9 && pro<=20) {
			return res = pro +" |> Frio";
		}else if (pro>20.9 && pro<=30) {
			return res = pro +" |> Normal";
		}else if (pro>30.9) {
			return res = pro +" |> Alta";
		}
		
		return res;
	}
	
	//Promedio Temperaturas Con Rangos de fecha
	public String promedioTemperaturasEspecifico(String fi,String ff) {
		Nodo nodoActual = nodoInicio;
		String res = "Error";
		int cont = 0;
		double sum = 0;
		double pro = 0;
		int diaI = Integer.parseInt(fi.substring(0, 2));
		int diaF = Integer.parseInt(ff.substring(0, 2));
		while (nodoActual != null) {
			if (diaI != diaF) {
				int a = Integer.parseInt(nodoActual.getDato().getFechaDeRegistro().substring(0,2));
				if ( a <= diaF) {
					sum = sum + nodoActual.getDato().getTemperatura();
					cont++;
				}
				diaI = diaI +1;
			}else {
				int a = Integer.parseInt(nodoActual.getDato().getFechaDeRegistro().substring(0,2));
				if ( a <= diaF) {
					sum = sum + nodoActual.getDato().getTemperatura();
					cont++;
				}
			}
			nodoActual = nodoActual.getEnlace();
		}
		pro = sum/cont;
		if (pro>0 && pro<=10) {
			return res = pro +" |> Congelacion";
		}else if (pro>10.9 && pro<=20) {
			return res = pro +" |> Frio";
		}else if (pro>20.9 && pro<=30) {
			return res = pro +" |> Normal";
		}else if (pro>30.9) {
			return res = pro +" |> Alta";
		}
		
		return res;
	}

}
